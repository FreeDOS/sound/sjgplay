SJGPLAY FOR DOS
===============

I am releasing source code to SJGPLAY for DOS Version 1.29 into the public domain.
This work is release under the Creative Commons CC0 license as of Feb 16, 2023.

Steve J. Gray
sjgray@rogers.com
http://6502.org/users/sjgray/
