# SJGPlay

Audio CDPlayer with Lyrics. Features syncronized lyrics & Karaoke! Easy lyric editor & syncronizer. Saves playlist for each CD. Big char display. Many display modes. Catalog/sort/print CD's and lyrics. Intro, repeat, button bars, waveform scope, freq analyzer, font editor, calendar, calculator, and screen saver. Supports SoundBlaster, multiple drives, 43/50-line screens, mouse and joystick. Now with SB Mixer and improved font support.


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## SJGPLAY.LSM

<table>
<tr><td>title</td><td>SJGPlay</td></tr>
<tr><td>version</td><td>1.29</td></tr>
<tr><td>entered&nbsp;date</td><td>2023-02-19</td></tr>
<tr><td>description</td><td>Audio CDPlayer with Lyrics</td></tr>
<tr><td>summary</td><td>Audio CDPlayer with Lyrics. Features syncronized lyrics & Karaoke! Easy lyric editor & syncronizer. Saves playlist for each CD. Big char display. Many display modes. Catalog/sort/print CD's and lyrics. Intro, repeat, button bars, waveform scope, freq analyzer, font editor, calendar, calculator, and screen saver. Supports SoundBlaster, multiple drives, 43/50-line screens, mouse and joystick. Now with SB Mixer and improved font support.</td></tr>
<tr><td>keywords</td><td>cdrom, music, player</td></tr>
<tr><td>author</td><td>Steve J. Gray</td></tr>
<tr><td>original&nbsp;site</td><td>http://www.6502.org/users/sjgray/software/sjgplay/sjgplay_dos.html</td></tr>
<tr><td>platforms</td><td>DOS (NASM), FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>Public Domain</td></tr>
</table>
